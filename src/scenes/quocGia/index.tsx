import * as React from 'react';
import QuocGiaStore from '../../stores/danhMuc/quocGiaStore';
import AppComponentBase from '../../components/AppComponentBase';
import { inject, observer } from 'mobx-react';
import { Card, Table, Row, Col, Button, Dropdown, Menu, Modal } from 'antd';
import { L } from '../../lib/abpUtility';
import { EntityDto } from '../../services/dto/entityDto';
import CreateOrUpdateQuocGia from './components/createOrUpdateQuocGia'
export interface IQuocGiaProps {
    quocGiaStore: QuocGiaStore;
}
export interface IQuocGiaState {
    modalId: number;
    modalVisible: boolean;

    maxResultCount: number;
    skipCount: number;
    sorting: string;
    filter: string;
}
const { confirm } = Modal;
@inject('quocGiaStore')
@observer
class QuocGia extends AppComponentBase<IQuocGiaProps, IQuocGiaState>  {
    formRef: any;
    state = {
        modalId: 0,
        modalVisible: false,
        maxResultCount: 2,
        sorting: 'id asc',
        skipCount: 0,
        filter: '',
    };

    async getAll() {
        this.props.quocGiaStore.getAllServerPaging(this.state)
    }
    async componentDidMount() {
        await this.getAll();
    }
    handleTableChange = (pagination: any, filters: any, sorter: any) => {
        if (sorter) {
            if (sorter.order === "ascend")
                this.setState({
                    sorting: sorter.field + " asc"
                })
            else if (sorter.order === "descend")
                this.setState({
                    sorting: sorter.field + " desc"
                })
        }
        this.setState(
            { skipCount: (pagination.current - 1) * this.state.maxResultCount!, maxResultCount: pagination.pageSize }, async () => await this.getAll());
    };
    async showCreateOrEditModal(input: EntityDto) {
        await this.props.quocGiaStore.getForEdit(input);
        this.setState({
            modalVisible: true,
            modalId: input.id,
        })


        if (input.id !== 0) {
            const { quocGiaModal } = this.props.quocGiaStore;
            var x = function(){
                return {...quocGiaModal}
            }
            this.formRef.props.form.setFieldsValue(x());
        }
        else {
            this.formRef.props.form.resetFields();
        }
    }

    handleSave = () => {
        const form = this.formRef.props.form;
        form.validateFields(async (err: any, values: any) => {
            if (err) {
                return;
            } else {

                await this.props.quocGiaStore.createOrUpdate({ id: this.state.modalId, ...values });
            }
            await this.getAll();
            this.setState({ modalVisible: false });
            form.resetFields();
        });
    }
    delete(input: EntityDto) {
        const self = this;
        confirm({
            title: 'Do you Want to delete these items?',
            onOk() {
                self.props.quocGiaStore.delete(input.id);
                self.getAll();
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    }
    render() {
        const { quocGias } = this.props.quocGiaStore
        const columns = [
            {

                title: L('Actions'),
                key: "action",
                width: 150,
                render: (text: string, item: any) => (
                    <div>
                        <Dropdown
                            trigger={['click']}
                            overlay={
                                <Menu>
                                    <Menu.Item onClick={() => this.showCreateOrEditModal({ id: item.id })}>{L('Edit')}</Menu.Item>
                                    <Menu.Item onClick={() => this.delete({ id: item.id })}>{L('Delete')}</Menu.Item>
                                </Menu>
                            }
                            placement="bottomLeft"
                        >
                            <Button type="primary" icon="setting">
                                {L('Actions')}
                            </Button>
                        </Dropdown>
                    </div>
                ),
            },
            {
                title: 'Tên quốc gia',
                dataIndex: 'tenQuocGia',
                key: 'tenQuocGia',
                sorter: true,

            },
            {
                title: 'Mã quốc gia',
                dataIndex: 'maQuocGia',
                key: 'maQuocGia',
                sorter: true,
            },
            {
                title: 'Mô tả',
                dataIndex: 'moTa',
                key: 'moTa',
                sorter: true,
            }

        ]
        return (
            <Card>
                <Row type="flex" justify="space-between">
                    <Col >
                        <h2>Danh sách quốc gia</h2>
                    </Col>
                    <Col >
                        <Button type="primary" icon="plus" onClick={() => this.showCreateOrEditModal({ id: 0 })}>Thêm mới</Button>
                    </Col>
                </Row>
                <Table
                    columns={columns}
                    size={'default'}
                    bordered={true}
                    rowKey={record => record.id.toString()}
                    dataSource={quocGias === undefined ? [] : quocGias.items}
                    pagination={{
                        pageSize: this.state.maxResultCount,
                        total: quocGias === undefined ? 0 : quocGias.totalCount,
                        defaultCurrent: 1, pageSizeOptions: ['2', '10', '20', '50'],
                        showSizeChanger: true
                    }}
                    loading={this.props.quocGiaStore.isLoading}
                    onChange={this.handleTableChange} />
                <pre>{JSON.stringify(this.props.quocGiaStore.quocGiaModal, null, 2)}</pre>
                <CreateOrUpdateQuocGia
                    wrappedComponentRef={(formRef: any) => {
                        this.formRef = formRef
                    }}
                    visible={this.state.modalVisible}
                    modalType={this.state.modalId ? "edit" : "'create'"}
                    onSave={this.handleSave}
                    onCancel={() =>
                        this.setState({
                            modalVisible: false,
                        })
                    }
                ></CreateOrUpdateQuocGia>
            </Card>
        );
    }
}

export default QuocGia;
