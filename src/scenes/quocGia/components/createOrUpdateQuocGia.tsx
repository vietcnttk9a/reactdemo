import * as React from 'react';
import { Checkbox, Form, Input, Modal } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import FormItem from 'antd/lib/form/FormItem';
import { L } from '../../../lib/abpUtility';
import TextArea from 'antd/lib/input/TextArea';

export interface ICreateOrUpdateQuocGiaProps extends FormComponentProps {
    visible: boolean;
    onCancel: () => void;
    onSave: () => void;
    modalType: string;
}
class CreateOrUpdateQuocGia extends React.Component<ICreateOrUpdateQuocGiaProps>
{
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Modal
                title={this.props.modalType === "edit" ? "Sửa thông tin quốc gia" : "Thêm mới quốc gia"}
                visible={this.props.visible}
                onOk={this.props.onSave}
                onCancel={this.props.onCancel}
            >
                <Form>
                    <FormItem label="Tên quốc gia" >
                        {getFieldDecorator('tenQuocGia', {
                            rules: [{
                                required: true,
                                message: "Giá trị nhập vào là bắt buộc!",
                            }]
                        })(<Input />)}
                    </FormItem>
                    <FormItem label="Mã quốc gia" >
                        {getFieldDecorator('maQuocGia',
                            {
                                rules: [{
                                    required: true,
                                    message: "Giá trị nhập vào là bắt buộc!",
                                }]
                            })(<Input />)}
                    </FormItem>
                    <FormItem label="Mã liên thông" >
                        {getFieldDecorator('niisId')(<Input type="number" />)}
                    </FormItem>
                    <FormItem label="Mô tả" >
                        {getFieldDecorator('moTa')(<TextArea />)}
                    </FormItem>

                    <FormItem label={L('IsActive')} >
                        {getFieldDecorator('isActive', { valuePropName: 'checked' })(<Checkbox>Kích hoạt</Checkbox>)}
                    </FormItem>
                </Form>
            </Modal>
        )
    }
}
export default Form.create<ICreateOrUpdateQuocGiaProps>()(CreateOrUpdateQuocGia);
