import { PagedFilterAndSortedRequest } from '../../../dto/pagedFilterAndSortedRequest';

export class QuocGiaDto {
    id!: number;
    tenQuocGia!: string;
    maQuocGia!: string;
    moTa!: string;
    isActive!: boolean;
    countrY_IMG_URL!: string;
    niisId!: number;
}
export interface QuocGiaInputDto extends PagedFilterAndSortedRequest {
    filter: string;
}