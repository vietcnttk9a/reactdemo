import { QuocGiaDto, QuocGiaInputDto } from './dto/quocGiaDto';
import http from '../../httpService';
import { EntityDto } from '../../dto/entityDto';
import { PagedResultDto } from '../../dto/pagedResultDto';

class QuocGiaService {
    public async getAllServerPaging(input: QuocGiaInputDto): Promise<PagedResultDto<QuocGiaDto>> {
        let result = await http.get('api/services/app/quocGia/getAllServerPaging', { params: input });
        return result.data.result;
    }
    public async createOrUpdate(input: QuocGiaDto) {
        let result = await http.post('api/services/app/quocGia/createOrUpdate', input);
        return result.data;
    }

    public async getForEdit(entityDto: EntityDto): Promise<QuocGiaDto> {
        let result = await http.get('api/services/app/quocGia/getForEdit', { params: entityDto });
        return result.data.result;
    }
    public async delete(id: number) {
        let result = await http.delete('api/services/app/quocGia/delete', { params: {id:id} });
        return result.data;
    }
    public async getAll(): Promise<QuocGiaDto[]> {
        let result = await http.get('api/services/app/quocGia/getAllToDDL');
        return result.data.result;
    }
}
export default new QuocGiaService()