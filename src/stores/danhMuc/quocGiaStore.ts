import { observable, action } from 'mobx';
import { QuocGiaInputDto, QuocGiaDto } from '../../services/danhMuc/quocGia/dto/quocGiaDto';
import { PagedResultDto } from '../../services/dto/pagedResultDto';
import quocGiaService from '../../services/danhMuc/quocGia/quocGiaService';
import { EntityDto } from '../../services/dto/entityDto';

class QuocGiaStore {

    @observable quocGiaModal: QuocGiaDto = new QuocGiaDto();
    @observable quocGias !: PagedResultDto<QuocGiaDto>;
    @observable isLoading: boolean = false;
    // constructor(){
    //     this.quocGiaModal = {
    //         id: 0,
    //         tenQuocGia: '',
    //         maQuocGia: '',
    //         moTa: '',
    //         isActive: false,
    //         countrY_IMG_URL: '',
    //         niisId: 0

    //     };
    // }

    @action
    async getAllServerPaging(input: QuocGiaInputDto) {
        this.isLoading = true;
        this.quocGias = await quocGiaService.getAllServerPaging(input);
        if (this.quocGias)
            this.isLoading = false;
    }
    @action
    async createOrUpdate(input: QuocGiaDto) {
        await quocGiaService.createOrUpdate(input);
    }
    @action
    async getForEdit(input: EntityDto) {
        this.quocGiaModal = await quocGiaService.getForEdit(input);
    }
    @action
    async delete(id: number) {
        await quocGiaService.delete(id);
    }
}
export default QuocGiaStore